﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HobbyCollection
{
    /// <summary>
    /// Представляет описание дисковой пилы
    /// </summary>
    [Serializable]
    public class CircularSaw : /*IComparer, IComparer<CircularSaw>,*/ IComparable<CircularSaw>, IComparable
    {
        #region Private Field

        private String manufacturerName;
        private String modelName;

        private Double cuttingDiskDiameter;
        private Double electricPower;
        private Double rpm;
        private Decimal price;

        #endregion

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public CircularSaw() { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="Manufacturer">Название производителя</param>
        /// <param name="Model">Номер модели</param>
        /// <param name="DiskDiameter">Диаметр режущего диска</param>
        /// <param name="Power">Мощность</param>
        /// <param name="RPM">Количество оборотов режущего диска</param>
        /// <param name="Price">Цена</param>
        public CircularSaw(string Manufacturer, string Model, Double DiskDiameter, Double Power, Double RPM, Decimal Price)
        {
            manufacturerName = Manufacturer;
            modelName = Model;

            cuttingDiskDiameter = DiskDiameter;
            electricPower = Power;
            rpm = RPM;

            price = Price;
        }

        /// <summary>
        /// Возвращает/устанавливает название производителя
        /// </summary>
        public String ManufacturerName
        {
            get { return manufacturerName; }
            set { manufacturerName = value; }
        }
        /// <summary>
        /// Возвращает/устанавливает название модели
        /// </summary>
        public String ModelName
        {
            get { return modelName; }
            set { modelName = value; }
        }

        /// <summary>
        /// Возвращает/устанавливает диаметр режущего диска
        /// </summary>
        public Double CuttingDiskDiameter
        {
            get { return cuttingDiskDiameter; }
            set { cuttingDiskDiameter = value; }
        }
        /// <summary>
        /// Возвращает/устанавливает значение электрической мощности
        /// </summary>
        public Double ElectricPower
        {
            get { return electricPower; }
            set { electricPower = value; }
        }
        /// <summary>
        /// Возвращает/устанавливает количество оборотов режущего диска
        /// </summary>
        public Double RPM
        {
            get { return rpm; }
            set { rpm = value; }
        }

        /// <summary>
        /// Возвращает/устанавливает значение цены
        /// </summary>
        public Decimal Price
        {
            get { return price; }
            set { price = value; }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //int IComparer<CircularSaw>.Compare(CircularSaw x, CircularSaw y)
        //{
        //    if (x.price.CompareTo(y.price) != 0)
        //    {
        //        return x.price.CompareTo(y.price);
        //    }
        //    else if (x.electricPower.CompareTo(y.electricPower) != 0)
        //    {
        //        return x.electricPower.CompareTo(y.electricPower);
        //    }
        //    else if (x.cuttingDiskDiameter.CompareTo(y.cuttingDiskDiameter) != 0)
        //    {
        //        return x.cuttingDiskDiameter.CompareTo(y.cuttingDiskDiameter);
        //    }
        //    else if (x.rpm.CompareTo(y.rpm) != 0)
        //    {
        //        return x.rpm.CompareTo(y.rpm);
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
      
        //public int Compare(object x, object y)
        //{
        //    CircularSaw a, b;
        //    if (x == null || y == null) 
        //    {
        //        throw new ArgumentException("Object is not a circle saw");
        //    }

        //    a = x as CircularSaw;
        //    if (a == null)
        //        throw new ArgumentException("Object is not a circle saw");

        //    b = y as CircularSaw;
        //    if (b == null)
        //        throw new ArgumentException("Object is not a circle saw");

        //    return this.Compare(a, b);
        //}

        /// <summary>
        /// Метод сравнения по умолчанию с другим объктом циркулярной пилы
        /// </summary>
        /// <param name="other">Сравниваемый объект циркулярной пилы</param>
        /// <returns></returns>
        public int CompareTo(CircularSaw other)
        {
            if (this.price.CompareTo(other.price) != 0)
            {
                return this.price.CompareTo(other.price);
            }
            else if (this.electricPower.CompareTo(other.electricPower) != 0)
            {
                return this.electricPower.CompareTo(other.electricPower);
            }
            else if (this.cuttingDiskDiameter.CompareTo(other.cuttingDiskDiameter) != 0)
            {
                return this.cuttingDiskDiameter.CompareTo(other.cuttingDiskDiameter);
            }
            else if (this.rpm.CompareTo(other.rpm) != 0)
            {
                return this.rpm.CompareTo(other.rpm);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Метод сравнения по умолчанию с любым объектом
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            CircularSaw a;
            if (obj == null)
            {
                throw new ArgumentException("Object is not a circle saw");
            }

            a = obj as CircularSaw;
            if (a == null)
                throw new ArgumentException("Object is not a circle saw");
            
            return this.CompareTo(a);
        }

        /// <summary>
        /// Возвращает развернутое описание экземпляра дисковой пилы
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Дисковая пила\n" +
                                 "Производитель: {0}\n" +
                                 "Модель: {1}\n" +
                                 "Мощность: {2} Вт\n" +
                                 "Диаметр режущего диска: {3} мм\n" +
                                 "Количество оборотов режущего диска: {4} об/мин\n" +
                                 "Цена: {5} грн\n", 
                                 manufacturerName,
                                 modelName,
                                 electricPower.ToString(),
                                 cuttingDiskDiameter.ToString(),
                                 rpm.ToString(),
                                 price.ToString());
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по цене
    /// </summary>
    public class CircularSawComparer_PriceOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.Price.CompareTo(y.Price);
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по мощности
    /// </summary>
    public class CircularSawComparer_PowerOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.ElectricPower.CompareTo(y.ElectricPower);
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по радиусу режущего диска
    /// </summary>
    public class CircularSawComparer_CuttingDiskDiameterOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.CuttingDiskDiameter.CompareTo(y.CuttingDiskDiameter);
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по оборотам режущего диска
    /// </summary>
    public class CircularSawComparer_RpmOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.RPM.CompareTo(y.RPM);
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по названию производителя
    /// </summary>
    public class CircularSawComparer_ManufacturerOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.ManufacturerName.CompareTo(y.ManufacturerName);
        }
    }

    /// <summary>
    /// Класс для сравнения объектов CircularSaw по названию модели
    /// </summary>
    public class CircularSawComparer_ModelOnly : Comparer<CircularSaw>
    {
        public override int Compare(CircularSaw x, CircularSaw y)
        {
            return x.ModelName.CompareTo(y.ModelName);
        }
    }

}
