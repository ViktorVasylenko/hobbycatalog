﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HobbyCollection
{
    public enum CollectionAction
    {
        Add = 1,
        Remove = 2
    }
    
    public class HobbyCollectionEventArgs:EventArgs
    {
        CollectionAction action;
        CircularSaw sawInAction;

        public HobbyCollectionEventArgs(CollectionAction Action, CircularSaw SawInAction)
        {
            action = Action;
            sawInAction = SawInAction;
        }

        public CollectionAction Action
        {
            get { return action; }
        }

        public CircularSaw SawInAction
        {
            get { return sawInAction; }
        }
    }

    /// <summary>
    /// Представляет коллекцию циркулярных пил
    /// </summary>
    [Serializable]
    public class CircularSawCollection: IList<CircularSaw>
    {
        private List<CircularSaw> innerList = new List<CircularSaw>();

        // ---------------------------------------------------------------------------------------------------

        public event EventHandler<HobbyCollectionEventArgs> CollectionChange;

        private void RaiseActionEvent(CollectionAction Action, CircularSaw SawInAction)
        {
            OnCollectionChange(new HobbyCollectionEventArgs(Action, SawInAction));
        }

        private void OnCollectionChange(HobbyCollectionEventArgs EventData)
        {
            EventHandler<HobbyCollectionEventArgs> storeEvent = CollectionChange;
            if(storeEvent!=null)
            {
                storeEvent(this, EventData);
            }
        }

        // ---------------------------------------------------------------------------------------------------

        public int IndexOf(CircularSaw item)
        {
            return innerList.IndexOf(item);
        }

        public void Insert(int index, CircularSaw item)
        {
            innerList.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            CircularSaw removedSaw = innerList[index];
            try
            {
                innerList.RemoveAt(index);
            }
            catch { return; }
            
            RaiseActionEvent(CollectionAction.Remove, removedSaw);
        }
        
        public void Add(CircularSaw item)
        {
            innerList.Add(item);
            RaiseActionEvent(CollectionAction.Add, item);
        }

        public void Clear()
        {
            innerList.Clear();
        }

        public bool Contains(CircularSaw item)
        {
            return innerList.Contains(item);
        }

        public void CopyTo(CircularSaw[] array, int arrayIndex)
        {
            innerList.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return innerList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public CircularSaw this[int index]
        {
            get
            {
                return innerList[index];
            }

            set
            {
                innerList[index] = value;
            }
        }

        public bool Remove(CircularSaw item)
        {
            bool res = false;

            res = innerList.Remove(item);
            if (res == true)
            {
                RaiseActionEvent(CollectionAction.Remove, item);
            }

            return res;
        }

        IEnumerator<CircularSaw> IEnumerable<CircularSaw>.GetEnumerator()
        {
            return innerList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return innerList.GetEnumerator();
        }

        /// <summary>
        /// Сортировка с параметрами по умолчанию
        /// </summary>
        public void Sort()
        {
            innerList.Sort();
        }

        /// <summary>
        /// Сортировка с заданными параметрами
        /// </summary>
        /// <param name="CircularSawComparer">Реализация интерфейса сортировки для CircularSaw</param>
        public void Sort(IComparer<CircularSaw> CircularSawComparer)
        {
            innerList.Sort(CircularSawComparer);
        }

        /// <summary>
        /// Сортировка с заданными параметрами в указанном диапазоне
        /// </summary>
        /// <param name="Index">Начальный индекс диапазона для сортировки</param>
        /// <param name="Count">Длина диапазона для сортировки</param>
        /// <param name="CircularSawComparer">Реализация интерфейса сортировки для CircularSaw</param>
        public void Sort(Int32 Index, Int32 Count, IComparer<CircularSaw> CircularSawComparer)
        {
            innerList.Sort(Index, Count, CircularSawComparer);
        }
    }

}
