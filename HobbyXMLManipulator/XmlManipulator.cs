﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HobbyCollection;
using System.Xml.Serialization;

namespace HobbyXMLManipulator
{
    public static class Constant_XML
    {
        public const string MainDirectoryPath = @"C:\TestXML\";
        public const string MainFileName = "SawsCollection.xml";

        public const string NodeName_MainCircularSaw = "CircularSaws";

        public const string NodeName_CircularSaw = "CircularSaw";
        public const string NodeName_Power = "ElectricPower";
        public const string NodeName_RPM = "RPM";
        public const string NodeName_CuttingDiskDiameter = "CuttingDiskDiameter";
        public const string NodeName_Price = "Price";

        public const string AttributeName_Manufacturer = "manufacturer";
        public const string AttributeName_ModelName = "model";
    }

    public class MyXmlWriter
    {
        private void CreateEmptyXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode mainNode;

            Directory.CreateDirectory(Constant_XML.MainDirectoryPath);
            FileStream fStream = File.Create(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName);
            fStream.Close();

            // *** Создаем узел верхнего уровня
            //XmlDeclaration decl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            //xmlDoc.InsertBefore(decl, xmlDoc.DocumentElement);
            mainNode = CreateMainNode(xmlDoc);
            xmlDoc.AppendChild(mainNode);

            xmlDoc.Save(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName);
        }

        private XmlNode CreateMainNode(XmlDocument Doc)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode mainSawNode;
            
            // *** Создаем узел верхнего уровня
            mainSawNode = Doc.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_MainCircularSaw, "");

            return mainSawNode;
        }

        private XmlNode CreateNode(CircularSaw Saw, XmlDocument CurrentDocument)
        {
            //XmlDocument xmlDoc = new XmlDocument();
            XmlNode newSawNode, powerNode, rpmNode, cutDiskRadiusNode, priceNode;

            // *** Создаем основной узел для всей пилы
            newSawNode = CurrentDocument.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_CircularSaw, "");
            
            XmlAttribute attrManufacturer = CurrentDocument.CreateAttribute(Constant_XML.AttributeName_Manufacturer);
            attrManufacturer.Value = Saw.ManufacturerName;
            XmlAttribute attrModel = CurrentDocument.CreateAttribute(Constant_XML.AttributeName_ModelName);
            attrModel.Value = Saw.ModelName;
            
            newSawNode.Attributes.Append(attrManufacturer);
            newSawNode.Attributes.Append(attrModel);

            // *** Создаем вложенный узел для мощности
            powerNode = CurrentDocument.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_Power, "");
            powerNode.InnerText = Saw.ElectricPower.ToString();
            newSawNode.AppendChild(powerNode);

            // *** Создаем вложенный узел для оборотов
            rpmNode = CurrentDocument.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_RPM, "");
            rpmNode.InnerText = Saw.RPM.ToString();
            newSawNode.AppendChild(rpmNode);

            // *** Создаем вложенный узел для диаметра диска
            cutDiskRadiusNode = CurrentDocument.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_CuttingDiskDiameter, "");
            cutDiskRadiusNode.InnerText = Saw.CuttingDiskDiameter.ToString();
            newSawNode.AppendChild(cutDiskRadiusNode);

            // *** Создаем вложенный узел для цены
            priceNode = CurrentDocument.CreateNode(XmlNodeType.Element, Constant_XML.NodeName_Price, "");
            priceNode.InnerText = Saw.Price.ToString();
            newSawNode.AppendChild(priceNode);

            return newSawNode;
        }

        public void WriteCollection(CircularSawCollection Saws)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode mainNode, newNode;

            //
            if (File.Exists(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName) == false)
            {
                CreateEmptyXML();
            }

            // Загружаем ХМЛ документ
            xmlDoc.Load(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName);
            //
            mainNode = xmlDoc.ChildNodes[0];
            mainNode.RemoveAll();

            // 
            foreach (CircularSaw curSaw in Saws)
            {
                // 
                newNode = CreateNode(curSaw, xmlDoc);
                
                // 
                mainNode.AppendChild(newNode);
            }
            // Переписываем файл настроек с внесенными изменениями
            try
            {
                xmlDoc.Save(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName);
            }
            catch { throw new Exception("Hе могу сохранить файл с данными коллекции в XML"); }
        }
    }

    public class MyXmlReader
    {
        private CircularSaw ReadOneSawNode(XmlNode SawNode)
        {
            CircularSaw saw = new CircularSaw();

            saw.ManufacturerName = SawNode.Attributes[Constant_XML.AttributeName_Manufacturer].Value;
            saw.ModelName = SawNode.Attributes[Constant_XML.AttributeName_ModelName].Value;
            // 
            foreach (XmlNode curParamNode in SawNode.ChildNodes)
            {
                // Создаем узел-элемент для записи нового АЦП
                if (curParamNode.LocalName == Constant_XML.NodeName_Power)
                    saw.ElectricPower = Convert.ToDouble(curParamNode.InnerText);
                else if(curParamNode.LocalName == Constant_XML.NodeName_RPM)
                    saw.RPM = Convert.ToDouble(curParamNode.InnerText);
                else if (curParamNode.LocalName == Constant_XML.NodeName_CuttingDiskDiameter)
                    saw.CuttingDiskDiameter = Convert.ToDouble(curParamNode.InnerText);
                else if (curParamNode.LocalName == Constant_XML.NodeName_Price)
                    saw.Price = Convert.ToDecimal(curParamNode.InnerText);
            }

            return saw;
        }

        public CircularSawCollection ReadCollection()
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode mainNode;
            CircularSawCollection readedSaws = new CircularSawCollection();
            CircularSaw curSaw = new CircularSaw();

            //
            if (File.Exists(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName) == false)
            {
                throw new FileNotFoundException("нет файла с данными в размещении по умолчанию");
            }

            // Загружаем ХМЛ документ
            xmlDoc.Load(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName);
            //
            mainNode = xmlDoc.ChildNodes[0];

            // 
            foreach (XmlNode curSawNode in mainNode.ChildNodes)
            {
                // 
                curSaw = ReadOneSawNode(curSawNode);

                // 
                readedSaws.Add(curSaw);
            }
            
            return readedSaws;
        }
    }

    public class StandartXmlSerializer
    {
        XmlSerializer serializer;
        //XmlWriter writer;

        public StandartXmlSerializer()
        {
            serializer = new XmlSerializer(typeof(CircularSawCollection));
            CreateDirectory();
        }

        private void CreateDirectory()
        {
            //
            if (Directory.Exists(Constant_XML.MainDirectoryPath) == false)
            {
                Directory.CreateDirectory(Constant_XML.MainDirectoryPath);
            }

            //writer = XmlWriter.Create(new FileStream(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName, FileMode.Create));
        }

        public bool Serialize(CircularSawCollection SawCollection)
        {
            try
            {
                using (FileStream fs = new FileStream(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName, FileMode.Create))
                {
                    XmlWriter writer = XmlWriter.Create(fs);
                    serializer.Serialize(writer, SawCollection);
                }
            }
            catch { return false; }

            return true;
        }
    }

    public class StandartXmlDeserializer
    {
        XmlSerializer serializer;

        public StandartXmlDeserializer()
        {
            serializer = new XmlSerializer(typeof(CircularSawCollection));
        }

        public CircularSawCollection Deserialize()
        {
            CircularSawCollection retCollection = new CircularSawCollection();

            if (File.Exists(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName) == false)
            {
                throw new FileNotFoundException(String.Format("Can not find file '{0}'", Constant_XML.MainDirectoryPath + Constant_XML.MainFileName));
            }

            using (FileStream fs = new FileStream(Constant_XML.MainDirectoryPath + Constant_XML.MainFileName, FileMode.Open))
            {
                retCollection = (CircularSawCollection)serializer.Deserialize(fs);
            }

            return retCollection;
        }

        public CircularSawCollection Deserialize(string PathToFile)
        {
            CircularSawCollection retCollection = new CircularSawCollection();

            if (File.Exists(PathToFile) == false)
            {
                throw new FileNotFoundException(String.Format("Can not find file '{0}'", PathToFile));
            }


            using (FileStream fs = new FileStream(PathToFile, FileMode.Open))
            {
                retCollection = (CircularSawCollection)serializer.Deserialize(fs);
            }

            return retCollection;
        }

    }
}
