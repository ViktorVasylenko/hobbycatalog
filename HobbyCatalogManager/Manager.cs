﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HobbyCollection;
using HobbyXMLManipulator;

namespace HobbyCatalogManager
{
    public class Manager
    {
        CircularSawCollection saws = new CircularSawCollection();
            
        /// <summary>
        /// Конструктор. Инициализирует начальное состояние коллекции
        /// </summary>
        public Manager()
        {
            FillCollection();
        }

        /// <summary>
        /// Инициализирует начальное состояние коллекции
        /// </summary>
        private void FillCollection()
        {
            CircularSaw curSaw;

            curSaw = new CircularSaw("Bosch", "GKS160", 160, 1050, 5600, 3100);
            saws.Add(curSaw);

            curSaw = new CircularSaw("Bosch", "GKS190", 190, 1600, 5500, 3600);
            saws.Add(curSaw);

            curSaw = new CircularSaw("DWT", "HKS 15-65", 184, 1500, 5000, 1850);
            saws.Add(curSaw);

            curSaw = new CircularSaw("Makita", "5604R", 165, 950, 5000, 3650);
            saws.Add(curSaw);

            curSaw = new CircularSaw("Makita", "HS7601", 190, 1200, 4900, 3100);
            saws.Add(curSaw);
        }

        /// <summary>
        /// Сортирует коллекцию по умолчанию
        /// </summary>
        public void SortDefault()
        {
            saws.Sort();
        }

        /// <summary>
        /// Сортирует коллекцию по цене
        /// </summary>
        public void SortByPrice()
        {
            saws.Sort(new CircularSawComparer_PriceOnly());
        }

        /// <summary>
        /// Сортирует коллекцию по производителю
        /// </summary>
        public void SortByManufakturer()
        {
            saws.Sort(new CircularSawComparer_ManufacturerOnly());
        }

        /// <summary>
        /// Сортирует коллекцию по номеру модели
        /// </summary>
        public void SortByModel()
        {
            saws.Sort(new CircularSawComparer_ModelOnly());
        }

        /// <summary>
        /// Сортирует коллекцию по мощности
        /// </summary>
        public void SortByPower()
        {
            saws.Sort(new CircularSawComparer_PowerOnly());
        }

        /// <summary>
        /// Сортирует коллекцию по количеству оборотов в минуту
        /// </summary>
        public void SortByRPM()
        {
            saws.Sort(new CircularSawComparer_RpmOnly());
        }

        /// <summary>
        /// Сортирует коллекцию по диметру пильного диска
        /// </summary>
        public void SortByCuttingDiskDiameter()
        {
            saws.Sort(new CircularSawComparer_CuttingDiskDiameterOnly());
        }

        /// <summary>
        /// Выводит данные в консоль о всех объектах в коллекции
        /// </summary>
        public void PrintSawsCollection()
        {
            StringBuilder strBuilder = new StringBuilder();
            String onlySawData = String.Empty;

            foreach(CircularSaw curSaw in saws)
            {
                onlySawData = String.Empty;
                onlySawData = curSaw.ToString();
                strBuilder.AppendLine(onlySawData);
            }

            Console.Write(strBuilder.ToString());
        }

        // ---------------------------------------------------------------------------------------------------

        public void SaveCollection()
        {
            MyXmlWriter writer = new MyXmlWriter();
            writer.WriteCollection(saws);
        }

        public void ReadCollection()
        {
            MyXmlReader reader = new MyXmlReader();
            saws = reader.ReadCollection();
        }

        public void SerializeCollection()
        {
            StandartXmlSerializer serializer = new StandartXmlSerializer();
            serializer.Serialize(saws);
        }

        public void DeserializeCollection()
        {
            StandartXmlDeserializer deserializer = new StandartXmlDeserializer();
            saws = deserializer.Deserialize();
        }

        // ---------------------------------------------------------------------------------------------------

        public void AddSomeSaw()
        {
            CircularSaw newSaw = new CircularSaw();
            //Encoding enc = Encoding.GetEncoding("koi8-r");
            //byte[] manuf = new byte[5];
            //byte[] model = new byte[8];

            Random r = new Random((int)DateTime.Now.ToFileTime());

            //r.NextBytes(manuf);
            //r.NextBytes(model);

            //newSaw.ManufacturerName = enc.GetString(manuf);
            //newSaw.ModelName = enc.GetString(model);
            newSaw.ManufacturerName = "TestManuf";
            newSaw.ModelName = "TestModelName";
            newSaw.ElectricPower = r.Next(1000, 2500);
            newSaw.CuttingDiskDiameter = r.Next(200, 350);
            newSaw.RPM = r.Next(2000, 4000);
            newSaw.Price = r.Next(2000, 5000);

            saws.Add(newSaw);
        }

        public void RemoveSaw(int SawIndex)
        {
            saws.RemoveAt(SawIndex);
        }

        public void SomeChange()
        {
            saws[3].ManufacturerName = "QWERTY";
            saws[3].Price = 0;
        }

        // ---------------------------------------------------------------------------------------------------

        public void SubscribeToHobbyCollectionAction()
        {
            saws.CollectionChange += ShowSawsCollectionChange;
        }

        public void UnSubscribeToHobbyCollectionAction()
        {
            saws.CollectionChange -= ShowSawsCollectionChange;
        }

        private void ShowSawsCollectionChange(object sender, HobbyCollectionEventArgs e)
        {
            StringBuilder strBuilder = new StringBuilder();
            String onlySawData = String.Empty;

            strBuilder.AppendLine("_____ Some action is throwing in the collection! _____\n");
            strBuilder.AppendLine(String.Format("\tAction type is: '{0}'\n", e.Action.ToString()));
            strBuilder.AppendLine("\tObject in action is:\n");
            onlySawData = e.SawInAction.ToString();
            strBuilder.AppendLine(onlySawData);

            Console.Write(strBuilder.ToString());
        }

        // ---------------------------------------------------------------------------------------------------

        public delegate bool Serialize<in T>(T InCollection) where T: IList<CircularSaw>;
        public delegate T DeSerialize<out T>() where T : IList<CircularSaw>;

        public void SerializeCollectionByDelegate()
        {
            StandartXmlSerializer serializer = new StandartXmlSerializer();
            Serialize<CircularSawCollection> serializeDelegate = new Serialize<CircularSawCollection>(serializer.Serialize);
            serializeDelegate(saws);
        }

        public void DeserializeCollectionByDelegate()
        {
            StandartXmlDeserializer deserializer = new StandartXmlDeserializer();
            DeSerialize<CircularSawCollection> deserializeDelegate = new DeSerialize<CircularSawCollection>(deserializer.Deserialize);
            saws = deserializeDelegate();
        }
    }

}
