﻿using HobbyCatalogManager;
using HobbyCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HobbyCatalog
{
    class Program
    {
        public static void ShowBanner(String BannerText)
        {
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("- " + BannerText);
            Console.WriteLine("------------------------------------------");
        }

        static void Main(string[] args)
        {
            Manager manager = new Manager();

            //ShowBanner("Несортированные данные");
            //manager.PrintSawsCollection();
            //Console.ReadKey(true);
            
            //ShowBanner("Сортировка по умолчанию (цена-> мощность-> диаметр-> обороты)");
            //manager.SortDefault();
            //manager.PrintSawsCollection();
            
            //ShowBanner("Сортировка по мощности");
            //manager.SortByPower();
            //manager.PrintSawsCollection();
            
            //ShowBanner("Сортировка по производителю");
            //manager.SortByManufakturer();
            //manager.PrintSawsCollection();
            
            //ShowBanner("Сортировка по диаметру диска");
            //manager.SortByCuttingDiskDiameter();
            //manager.PrintSawsCollection();
            //Console.ReadKey(true);
            
            //ShowBanner("Изменили данные. Сортировка по умолчанию (цена-> мощность-> диаметр-> обороты)");
            //manager.SomeChange();
            //manager.SortDefault();
            //manager.PrintSawsCollection();
            //Console.ReadKey(true);
            
            //ShowBanner("Сохранение данных в XML файл");
            //try
            //{
            //    manager.SaveCollection();
            //    Console.WriteLine("Done");
            //}
            //catch { Console.WriteLine("Fail"); }
            
            //ShowBanner("Данные, прочитанные из XML файла");
            //try
            //{
            //    manager.ReadCollection();
            //    manager.PrintSawsCollection();
            //}
            //catch { Console.WriteLine("Fail"); }
            //Console.ReadKey(true);

            ShowBanner("Сериализация данных в XML файл");
            try
            {
                manager.SerializeCollection();
                Console.WriteLine("Done");
            }
            catch { Console.WriteLine("Fail"); }
            Console.ReadKey(true);
            
            ShowBanner("Данные, десериализованные из XML файла");
            try
            {
                manager.DeserializeCollection();
                manager.PrintSawsCollection();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Fail\n" + exc.Message);
            }
            Console.ReadKey(true);
            
            ShowBanner("Изменили данные. Сортировка по умолчанию (цена-> мощность-> диаметр-> обороты)");
            manager.SomeChange();
            manager.SortDefault();
            manager.PrintSawsCollection();
            Console.ReadKey(true);

            Console.Clear();
            ShowBanner("Сериализация данных в XML файл");
            try
            {
                manager.SerializeCollection();
                Console.WriteLine("Done");
            }
            catch { Console.WriteLine("Fail"); }
            Console.ReadKey(true);
            
            ShowBanner("Данные, десериализованные из XML файла");
            try
            {
                manager.DeserializeCollection();
                manager.PrintSawsCollection();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Fail\n" + exc.Message);
            }
            Console.ReadKey(true);

            Console.Clear();
            ShowBanner("Подписываемся на получение событий от коллекции");
            manager.SubscribeToHobbyCollectionAction();
            manager.AddSomeSaw();
            manager.AddSomeSaw();
            manager.RemoveSaw(4);
            manager.RemoveSaw(2);
            Console.ReadKey(true);

            Console.Clear();
            ShowBanner("Отписываемся от получения событий от коллекции (добавим и удалим по 2 элемента коллеции)");
            manager.UnSubscribeToHobbyCollectionAction();
            manager.AddSomeSaw();
            manager.AddSomeSaw();
            manager.RemoveSaw(1);
            manager.RemoveSaw(5);
            Console.WriteLine(@"- Финальный вид коллеции после добавления\удаления");
            manager.PrintSawsCollection();
            Console.ReadKey(true);

            Console.Clear();
            ShowBanner("Сериализация данных в XML файл (через делегат)");
            try
            {
                manager.SerializeCollectionByDelegate();
                Console.WriteLine("Done");
            }
            catch { Console.WriteLine("Fail"); }
            Console.ReadKey(true);
            
            ShowBanner("Данные, десериализованные из XML файла (через делегат)");
            try
            {
                manager.DeserializeCollectionByDelegate();
                manager.PrintSawsCollection();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Fail\n" + exc.Message);
            }
            Console.ReadKey(true);

        }

    }
}
